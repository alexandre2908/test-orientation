<?php
session_start();
    require ("App/Controllers/Session_Control.php");
    Session_Control::verify_membre_logged();
?>

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from mdbootstrap.com/live/_MDB/templates/Ecommerce/home-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:19:54 GMT -->
<head>
     <!-- Required meta tags always come first -->
     <meta charset="utf-8">
     
     <?php include "Pages/Includes/Css_Include.php";?>

</head>

<body>
<!--Double navigation-->
<header>
     
     <!-- Navbar -->
     <?php include "Pages/Includes/Navbar_Admin.php";?>
     <!-- /.Navbar -->

</header>
<!-- /.Double navigation -->







<?php
include("Pages/Includes/Admin.Footer.php");
?>
<!-- /.Footer -->

<?php
include("Pages/Includes/scripts.php");
?>
</body>
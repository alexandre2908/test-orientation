<?php
    session_start();
    require ("App/Controllers/Session_Control.php");
?>


<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from mdbootstrap.com/live/_MDB/templates/Ecommerce/home-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:19:54 GMT -->
<head>
     <!-- Required meta tags always come first -->
     <meta charset="utf-8">
     
     <?php include "Pages/Includes/Css_Include.php";?>

</head>

<body>
<!--Double navigation-->
<header>
     
     <!-- Navbar -->
     <?php include "Pages/Includes/Navbar.php";?>
     <!-- /.Navbar -->

    <!-- Intro Section -->
    <div id="home" class="view hm-black-strong-1 jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('css/Images/livre.jpeg');">
        <div class="full-bg-img">
            <div class="container flex-center">
                <div class="row smooth-scroll">
                    <div class="col-md-12 white-text text-center">
                        <div class="wow fadeInDown" data-wow-delay="0.2s">
                            <h2 class="display-3 font-bold mb-2 blue-text">Orientation</h2>
                            <hr class="hr-light">
                            <h3 class="subtext-header mt-4 mb-5 text-gray-dark">Besoins De Conseil Pour Savoir Quel Chemins Prendre Dans ces Etudes? </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>
<!-- /.Double navigation -->
<main>

    <div class="container">

        <!--Section: About-->
        <section id="about" class="section mt-4 mb-2">

            <!--Secion heading-->
            <h1 class="section-heading my-5 py-5 font-bold wow fadeIn" data-wow-delay="0.2s">A Propos de Nous</h1>

            <!--First row-->
            <div class="row">

                <!--First column-->
                <div class="col-lg-5 col-md-12 mb-5 pb-4 wow fadeIn" data-wow-delay="0.4s">

                    <!--Image-->
                    <img src="css/Images/carousel.jpg" class="img-fluid z-depth-1" alt="My photo">

                </div>
                <!--First column-->

                <!--Second column-->
                <div class="col-lg-6 ml-lg-auto col-md-12 wow fadeIn" data-wow-delay="0.4s">

                    <!--Description-->
                    <p align="justify" class="grey-text">Ce test est gratuit et conçu  à
                        partir des modèles d’évaluation les plus
                        largement éprouvés. Il comporte 3 parties
                        indépendantes évaluant, les intérêts professionnels,
                        les motivations et la personnalité. A partir des résultats
                        combinés de ces trois tests, une liste de métiers vous est
                        proposée en fonction des probabilités de réussite dans chacun
                        d’entre eux. Ce test constitue un outil d’aide à l’orientation
                        et n’a pas l’ambition de décider d’un chemin tout tracé !</p>

                </div>
                <!--Second column-->

            </div>
            <!--First row-->

        </section>
        <!--Section: About-->
        
        <hr class="mt-5">
        <!--Section: About-->
        <section id="about" class="section mt-4 mb-2">

            <!--Secion heading-->
            <h1 class="section-heading my-5 py-5 font-bold wow fadeIn" data-wow-delay="0.2s">Comment ca Marche</h1>

            <!--First row-->
            <div class="row">

                <!--First column-->
                <div class="col-lg-5 col-md-12 mb-5 pb-4 wow fadeIn" data-wow-delay="0.4s">

                    <!--Image-->
                    <img src="css/Images/carousel.jpg" class="img-fluid z-depth-1" alt="My photo">

                </div>
                <!--First column-->

                <!--Second column-->
                <div class="col-lg-6 ml-lg-auto col-md-12 wow fadeIn" data-wow-delay="0.4s">

                    <!--Description-->
                    <p align="justify" class="grey-text">
                        Premierement vous devez vous <a href="Register.php" class=""><strong class="h4">enregistrer</strong></a>
                        pour que nos conseilliers aient un meilleur suivi de vos aptitudes.
                        puis une fois authentifie vous pourrez <a href="Test_Orientation.php" class=""><strong class="h4">Passer Le Test</strong></a>
                        et suivre la procedure apres
                    </p>
                </div>
                <!--Second column-->

            </div>
            <!--First row-->

        </section>
        <!--Section: About-->

        <hr class="s">
        

        <hr class="mt-5">
        
    </div>

</main>


<?php
include("Pages/Includes/Footer.php");
?>
<!-- /.Footer -->



<?php
include("Pages/Includes/scripts.php");
?>
</body>
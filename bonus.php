<?php
session_start();
require ("App/Controllers/Session_Control.php");
require ("App/Models/Database_Connections.php");
require ("App/Models/Database_Operations.php");
$operation = new Database_Operations();
$number = $_GET['n'];
$all_question = $operation->getQuetionsBonusById($number);
$responses = $operation->getResponsesByIdQuestionsBonus($all_question["id_bonus"]);
$id_candidat = Session_Control::return_candidat_id();
$number_question = $operation->getNumberOfBonusQuestions();
?>


<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from mdbootstrap.com/live/_MDB/templates/Ecommerce/home-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:19:54 GMT -->
<head>
     <!-- Required meta tags always come first -->
     <meta charset="utf-8">
     
     <?php include "Pages/Includes/Css_Include.php";?>

</head>

<body>
<!--Double navigation-->
<header>
     
     <!-- Navbar -->
     <?php include "Pages/Includes/Navbar.php";?>
     <!-- /.Navbar -->

</header>
<!-- /.Double navigation -->

<div class="container mt-5">
     <div class="jumbotron">
          <h3 class="h3-responsive text-center">Question numero <?php echo $number."/".$number_question?>:</h3>
          <form class="form-group ml-5 mt-2" method="post" action="process_bonus.php">
               <h4 class="text-center"><?php print_r( $all_question["questions_bonus"])?></h4>
               <?php
               $i = 1;
               foreach ($responses as $respons){
                    ?>
                    <div class="form-group">
                         <input name="reponse_choisit" type="radio" id="choix<?php echo $i ?>" value="<?php echo $respons["id_reponse_bonus"]?>" checked>
                         <label for="choix<?php echo $i ?>"><?php echo $respons["reponse_bonus"] ?></label>
                    </div>
                    <?php
                    $i++;
               }?>
               
               <button class="btn btn-success ml-5" type="submit">Suivant</button>
               <input type="hidden" name="number" value="<?php echo $number; ?>" />
          </form>
     </div>
</div>

<?php
include("Pages/Includes/Footer.php");
?>
<!-- /.Footer -->



<?php
include("Pages/Includes/scripts.php");
?>
</body>
<?php
session_start();
require ("App/Controllers/Session_Control.php");
require ("App/Models/Database_Connections.php");
require ("App/Models/Database_Operations.php");
$operation = new Database_Operations();
$number = $_GET['n'];
$all_question = $operation->getQuetionsById($number);
$responses = $operation->getResponsesByIdQuestion($all_question["id_questions"]);
$id_candidat = Session_Control::return_candidat_id();
?>


<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from mdbootstrap.com/live/_MDB/templates/Ecommerce/home-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:19:54 GMT -->
<head>
     <!-- Required meta tags always come first -->
     <meta charset="utf-8">
     
     <?php include "Pages/Includes/Css_Include.php";?>

</head>

<body>
<!--Double navigation-->
<header>
     
     <!-- Navbar -->
     <?php include "Pages/Includes/Navbar.php";?>
     <!-- /.Navbar -->

</header>
<!-- /.Double navigation -->

    <div class="container mt-5">
         <div class="jumbotron">
              <h3 class="h3-responsive text-center">Question numero <?php echo $number ."/".Database_Operations::LIMIT_QUESTION?> :</h3>
              <form class="form-group ml-5 mt-2" method="post" action="process.php">
                   <h4 class="h4-responsive text-center"><?php echo $all_question["questions"]?></h4>
                   <?php
                   $i = 1;
                   foreach ($responses as $respons){
                        ?>
                        <div class="form-group">
                             <input name="reponse_choisit" type="radio" id="choix<?php echo $i ?>" value="<?php echo $respons["id_reponse"]?>" checked>
                             <label for="choix<?php echo $i ?>"><?php echo $respons["reponse"] ?></label>
                        </div>
                   <?php
                        $i++;
                   }?>
                   
                   <button class="btn btn-success ml-5" type="submit">Suivant</button>
                   <input type="hidden" name="number" value="<?php echo $number; ?>" />
              </form>
         </div>
    </div>

<?php
include("Pages/Includes/Footer.php");
?>
<!-- /.Footer -->



<?php
include("Pages/Includes/scripts.php");
?>
</body>
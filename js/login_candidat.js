$(document).ready(function () {

    $("#btn_login").click(function () {
        var email_candidat = $("#txt_email").val();
        var password_candidat = $("#txt_password").val();
        $.post("App/Controllers/login_candidat_verify.php", {email_candidat : email_candidat, password_candidat: password_candidat}, function (data) {
            if (data === "null"){
                toastr.error("L'Email Ou Mots De Passe Incorrect Veuillez Ressayer", 'Connection');
            }else if (data === "done"){
                window.location.replace("index.php");
            }
        })
    });
});
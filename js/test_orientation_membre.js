var openmodalresultat = function (id_test, resultat) {
    document.getElementById("list_correspondance").innerHTML = "";
    $.post("App/Controllers/return_correspondance.php", {resultat:resultat}, function (data) {
        data = jQuery.parseJSON(data);
        for (var i=0; i< data.length; i++){
           $("#list_correspondance").after("<li class=\"list-group-item\">"+data[i]+" <li/>");
        }
    });
    $.post("App/Controllers/return_conseillier_resultat.php", {id_test:id_test}, function (data) {
        if (data === "null"){
        }else if(data ==="none"){
            $("#resultat_conseil").text("Le Conseilliers n'a pas encore publier le resultat");
        }else{
            $("#resultat_conseil").text(data);
        }
    });
    $("#id_test_conseillier_take").val(id_test);

    $("#centralModalInfo").modal("show");

};
$("#modal_envoyer").click(function () {
    var id_test = $("#id_test_conseillier_take").val();
    var resultat = $("#resultat_candidat").val();
    $.post("App/Controllers/insert_resultat_conseillier.php", {id_test:id_test,resultat:resultat}, function (data) {
        if (data === "done"){
            toastr.success("Resultat Enregistre", 'Message');
            $("#centralModalInfo").modal("hide");
        }else {
            toastr.success("Erreur lors de L'enregistrement veuillez reessayer", 'Message');
        }
    });
});

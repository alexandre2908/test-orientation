var actualiser_messages = function () {
     var list_item = $("#list_chat");
    var id_candiat = $("#id_candidat").val();
    var id_conseillier = $("#id_conseillier").val();
  $.post("App/Controllers/all_message_id.php", {id_candidat: id_candiat, id_conseillier:id_conseillier},
      function (data) {
      data = jQuery.parseJSON(data);
        for (var i=0; i< data.length; i++){
            if (data[i].type_message ==="candidat"){
                var list_declare = "<li class=\"list-group-item \" >";
                var links_date = "<a  href=\"#\" style=\"float:left\" >"+data[i].message+"</a><br/><br/><br/>"+"\n"+
                    "<p class=\"mr-auto\" style=\"float: left\">"+data[i].dateHeure_message+"" +
                    " Par <span class=\"text-danger\"> Le candidat </span></p>";

            }else {
                var list_declare = "<li class=\"list-group-item secondary-color\" style='border-radius: 3%'>";
                var links_date = "<a  href=\"#\" style=\"float:right\"><span class='text-white'>"+data[i].message+"</span></a><br/><br/><br/>"+"\n"+
                    "<p class=\"mr-auto\" style=\"float: right\">"+data[i].dateHeure_message+"" +
                    " Par <span class=\"text-white\"> vous </span></p>";
            }
            var list_close = "</li>";

            var all_compile =all_compile+ (list_declare + links_date + list_close);
        }
          list_item.html(all_compile);
      });
};

$("#btn_envoyer").click(function () {
    var message_chat = $("#message_chat").val();
    var id_candiat = $("#id_candidat").val();
    var id_conseillier = $("#id_conseillier").val();
    $.post("App/Controllers/insert_new_chat_membre.php", {id_candidat: id_candiat, message:message_chat, id_conseillier:id_conseillier}, function (data) {
        if (data === "done"){
            toastr.success("Message Envoye Avec Succes", 'Message');
            actualiser_messages();

        }else if (data === "null"){
            toastr.error("Erreur lors de l'envoie du message", 'Message');
        }
    });
    $("#message_chat").val(" ");
});

$("#modal_envoyer").click(function () {
    var resultat = $("#resultat_candidat").val();
    $.post("App/Controllers/insert_resultat_conseillier.php", {resultat:resultat}, function (data) {
        if (data === "done"){
            toastr.success("Resultat Enregistrer avec success", 'Message');

        }else if (data === "null"){
            toastr.error("Erreur lors de l'enregistrement du resultat", 'Message');
        }
    });

});
actualiser_messages();

window.setInterval(function () {
    actualiser_messages();
}, 5000);
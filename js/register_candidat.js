$(document).ready(function () {

    $("#btn_register").click(function () {
        var email_candidat = $("#txt_email").val();
        var password_candidat = $("#txt_password").val();
        var nom_complet = $("#txt_nom").val();
        $.post("App/Controllers/register_candidat.php", {email_candidat : email_candidat, password_candidat: password_candidat, nom_complet:nom_complet}, function (data) {
            if (data === "null"){
                toastr.error("Erreur lors de l'enregistrement", 'Enregistrement');
            }else if (data === "done"){
                toastr.success("Enregistrement Reussi", 'Enregistrement');
                window.location.replace("Login.php");
            }else {
                alert(data);
            }
        })
    });
});
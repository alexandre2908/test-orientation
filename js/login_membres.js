$(document).ready(function () {

    $("#btn_login").click(function () {
        var username_membre = $("#txt_username").val();
        var password_membre = $("#txt_password").val();
        $.post("App/Controllers/login_membre_verify.php", {username_membre : username_membre, password_membre: password_membre}, function (data) {
            if (data === "null"){
                toastr.error('Username Ou Mots De Passe Incorrect Veuillez Ressayer', 'Connection');
            }else if (data === "done"){
                window.location.replace("Membre.index.php");
            }else {
                alert(data);
            }
        })
    });
});
<?php
session_start();
require ("App/Controllers/Session_Control.php");
require ("App/Models/Candidat.php");
require ("App/Models/Database_Connections.php");
require ("App/Models/Database_Operations.php");
$operation = new Database_Operations();
$all_candidat = $operation->list_Candidat();
Session_Control::verify_membre_logged();

?>

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from mdbootstrap.com/live/_MDB/templates/Ecommerce/home-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:19:54 GMT -->
<head>
     <!-- Required meta tags always come first -->
     <meta charset="utf-8">
     
     <?php include "Pages/Includes/Css_Include.php";?>

</head>

<body>
<!--Double navigation-->
<header>
     
     <!-- Navbar -->
     <?php include "Pages/Includes/Navbar_Admin.php";?>
     <!-- /.Navbar -->

</header>
<!-- /.Double navigation -->



<div class="container mt-3">
     <h2 class="text-center text-gray-dark">Veuillez Selectionner Un Candidat Pour Voir ces Tests</h2>
</div>

<div class="row ml-3">
     <?php
     foreach ($all_candidat as $candidat){
          ?>
          <div class="col-3 mt-5">
               <div class="card testimonial-card">
                    
                    <!--Bacground color-->
                    <div class="card-up indigo lighten-1">
                    </div>
                    
                    <!--Avatar-->
                    <div class="avatar"><img src="" class="rounded-circle">
                    </div>
                    
                    <div class="card-body">
                         <!--Name-->
                         <h4 class="card-title"><?php echo $candidat->getNomComplet()?></h4>
                         <hr>
                         <!--Quotation-->
                         <a class="btn btn-success" href="Testadminview.php?candidat=<?php echo $candidat->getIdCandidat()?>"> Voir Test Du Candidat</a>
                    </div>
               
               </div>
          </div>
          <?php
     }
     ?>

</div>



<?php
include("Pages/Includes/Admin.Footer.php");
?>
<!-- /.Footer -->

<?php
include("Pages/Includes/scripts.php");
?>
</body>
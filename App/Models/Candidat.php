<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 05/09/2018
 * Time: 13:03
 */

class Candidat
{
     /**
      * @var
      */
     private $id_candidat;
     /**
      * @var
      */
     private $nom_complet;
     /**
      * @var
      */
     private $email_candidat;
     /**
      * @var
      */
     private $password_candidat;
     
     /**
      * @return mixed
      */
     public function getIdCandidat()
     {
          return $this->id_candidat;
     }
     
     /**
      * @param mixed $id_candidat
      */
     public function setIdCandidat($id_candidat)
     {
          $this->id_candidat = $id_candidat;
     }
     
     /**
      * @return mixed
      */
     public function getNomComplet()
     {
          return $this->nom_complet;
     }
     
     /**
      * @param mixed $nom_complet
      */
     public function setNomComplet($nom_complet)
     {
          $this->nom_complet = $nom_complet;
     }
     
     /**
      * @return mixed
      */
     public function getEmailCandidat()
     {
          return $this->email_candidat;
     }
     
     /**
      * @param mixed $email_candidat
      */
     public function setEmailCandidat($email_candidat)
     {
          $this->email_candidat = $email_candidat;
     }
     
     /**
      * @return mixed
      */
     public function getPasswordCandidat()
     {
          return $this->password_candidat;
     }
     
     /**
      * @param mixed $password_candidat
      */
     public function setPasswordCandidat($password_candidat)
     {
          $this->password_candidat = $password_candidat;
     }
     
     
     
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 30/08/2018
 * Time: 16:03
 */

class Database_Connections
{
     const DATABASE_TYPE = "mysql";
      const DATABASE_HOST = "localhost";
      const DATABASE_NAME = "test_orientation";
      const DATABASE_USERNAME = "root";
      const DATABASE_PASSWORD = "";
      const DATABASE_DNS =
           Database_Connections::DATABASE_TYPE.":host=".
           Database_Connections::DATABASE_HOST.";dbname=".
           Database_Connections::DATABASE_NAME;
      public static $connection = NULL;
     
     public static function Get_Connection():PDO
     {
       if (is_null(Database_Connections::$connection)){
            Database_Connections::$connection = new PDO(Database_Connections::DATABASE_DNS,
                 Database_Connections::DATABASE_USERNAME,
                 Database_Connections::DATABASE_PASSWORD);
            return Database_Connections::$connection;
       }else{
            return Database_Connections::$connection;
       }
      }
}
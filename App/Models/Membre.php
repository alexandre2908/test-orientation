<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 05/09/2018
 * Time: 12:56
 */


class Membre
{
     /**
      * @var
      */
     private $id_membres;
     /**
      * @var
      */
     private $username_membres;
     /**
      * @var
      */
     private $password_membres;
     /**
      * @var
      */
     private $type_membres;
     
     /**
      * @var
      */
     private $etat_membre;
     /**
      * @var
      */
     private $nom_membre;
     
     
     //Getters and Setters
     
     
     /**
      * @return mixed
      */
     public function getIdMembres()
     {
          return $this->id_membres;
     }
     
     /**
      * @param mixed $id_membres
      */
     public function setIdMembres($id_membres)
     {
          $this->id_membres = $id_membres;
     }
     
     /**
      * @return mixed
      */
     public function getUsernameMembres()
     {
          return $this->username_membres;
     }
     
     /**
      * @param mixed $username_membres
      */
     public function setUsernameMembres($username_membres)
     {
          $this->username_membres = $username_membres;
     }
     
     /**
      * @return mixed
      */
     public function getPasswordMembres()
     {
          return $this->password_membres;
     }
     
     /**
      * @param mixed $password_membres
      */
     public function setPasswordMembres($password_membres)
     {
          $this->password_membres = $password_membres;
     }
     
     /**
      * @return mixed
      */
     public function getTypeMembres()
     {
          return $this->type_membres;
     }
     
     /**
      * @param mixed $type_membres
      */
     public function setTypeMembres($type_membres)
     {
          $this->type_membres = $type_membres;
     }
     
     /**
      * @return mixed
      */
     public function getEtatMembre()
     {
          return $this->etat_membre;
     }
     
     /**
      * @param mixed $etat_membre
      */
     public function setEtatMembre($etat_membre)
     {
          $this->etat_membre = $etat_membre;
     }
     
     /**
      * @return mixed
      */
     public function getNomMembre()
     {
          return $this->nom_membre;
     }
     
     /**
      * @param mixed $nom_membre
      */
     public function setNomMembre($nom_membre)
     {
          $this->nom_membre = $nom_membre;
     }
     
     
     
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 30/08/2018
 * Time: 21:42
 */

require ("Chat.php");

class Database_Operations
{
     const LIMIT_QUESTION = 13;
     /**
      * @var PDO
      */
     private $connection;
     
     public function __construct()
     {
          $this->connection = Database_Connections::Get_Connection();
     }
     
     public function getOne_membre($membre_username, $password_membre){
          $request = $this->connection->query("SELECT * FROM membres WHERE username_membre= '$membre_username' AND password_membre='$password_membre'");
          $donnees = $request->fetchAll(PDO::FETCH_ASSOC);
          return $donnees;
     }
     public function setMembreActif($id_membre){
          return $this->connection->exec("UPDATE membres SET etat_membre=1 WHERE id_membres='$id_membre'" );
     }
     public function setMembreInActif($id_membre){
          $this->connection->exec("UPDATE membres SET etat_membre=0 WHERE id_membres='$id_membre'" );
     }
     public function getOne_candidat($membre_username, $password_membre){
          $request = $this->connection->query("SELECT * FROM candidat WHERE email_candidat= '$membre_username' AND password_candidat='$password_membre'");
          $donnees = $request->fetchAll(PDO::FETCH_ASSOC);
          return $donnees;
     }
     public function getOne_candidat_ById($id_candidat){
          $request = $this->connection->query("SELECT * FROM candidat WHERE id_candidat='$id_candidat'");
          $donnees = $request->fetch(PDO::FETCH_ASSOC);
          return $donnees;
     }
     
     public function list_membres_connectes(){
          $request = $this->connection->query("SELECT * FROM membres WHERE etat_membre=1");
          $donnees = $request->fetchAll(PDO::FETCH_CLASS,Membre::class);
          return $donnees;
     }
     public function list_Candidat(){
          $request = $this->connection->query("SELECT * FROM candidat");
          $donnees = $request->fetchAll(PDO::FETCH_CLASS,Candidat::class);
          return $donnees;
     }
     public function gall_Correspondences($libele){
          $request = $this->connection->prepare("SELECT correspondance FROM profiles WHERE libele_profile='$libele'");
          $request->execute();
          $donnees = $request->fetchAll(PDO::FETCH_ASSOC);
          return $donnees;
     }
     public function getresultat_conseillier($id_test){
          $request = $this->connection->prepare("SELECT test_orientation.test_orientation.conseille_conseilier FROM test_orientation WHERE test_orientation.test_orientation.id_test='$id_test'");
          $request->execute();
          $donnees = $request->fetchAll(PDO::FETCH_ASSOC);
          return $donnees;
     }
     public function nombre_message_candidat_non_lus($id_candidat, $id_conseillier){
          $request = $this->connection->query("SELECT count(*) as total FROM chat WHERE id_candidat='$id_candidat' AND id_conseiller='$id_conseillier' AND lus_conseiller=0");
          $donnees = $request->fetch(PDO::FETCH_ASSOC);
          return $donnees['total'];
          
     }
     
     
     public function login_verify_membres($username_membres, $password_membres): bool {
          $response = FALSE;
          $request = $this->connection->query("SELECT * FROM membres WHERE username_membre= '$username_membres' AND password_membre='$password_membres'");
          $count = $request->rowCount();
          if ($count>0){
               $response = TRUE;
          }
          return $response;
     }
     public function login_verify_candidats($user_email, $user_password): bool {
          $response = FALSE;
          $request = $this->connection->query("SELECT * FROM candidat WHERE email_candidat= '$user_email' AND password_candidat='$user_password'");
          $count = $request->rowCount();
          if ($count>0){
               $response = TRUE;
          }
          return $response;
     }
     public function register_Candidat(Candidat $candidat):bool {
          
          $statement = $this->connection->prepare("INSERT INTO candidat (nom_complet, email_candidat, password_candidat) VALUES (:nom_complet, :email_candidat, :passsword_candidat)");
          $binded_values = array(
               "nom_complet" => $candidat->getNomComplet(),
               "email_candidat" => $candidat->getEmailCandidat(),
               "passsword_candidat" => $candidat->getPasswordCandidat()
          );
          $this->PDOBindArray($statement, $binded_values);
          
          return $statement->execute();
     }
     
     public function register_membres(Membre $membre): bool {
          $statement = $this->connection->prepare("INSERT INTO membres (username_membre, password_membre, type_membre) VALUES (:username_membre, :password_membre, :type_membre)");
          $binded_values = array(
               "username_membre" => $membre->getUsernameMembres(),
               "password_membre" => $membre->getPasswordMembres(),
               "type_membre" =>$membre->getTypeMembres()
          );
          $this->PDOBindArray($statement, $binded_values);
     
          return $statement->execute();
     }
     public function insert_arrayQuestions(array $questions): bool {
          
          foreach ($questions as $current){
               $statement = $this->connection->prepare("INSERT INTO questions (questions) VALUES (:question)");
               $binded_values = array(
                    "question" => $current
               );
               $this->PDOBindArray($statement, $binded_values);
               $resultat = $statement->execute();
          }
          return $resultat;
     }
     public function insert_arrayResponse(array $responses): bool {
          
          foreach ($responses as $current){
               $statement = $this->connection->prepare("INSERT INTO reponses (reponse, id_questions, niveau_reponse) VALUES (:reponse, :id_question, :niveau_reponse)");
               $binded_values = array(
                    "reponse" => $current["reponse"],
                    "id_question" =>$current["id_question"],
                    "niveau_reponse" => $current["niveau_reponse"]
               );
               $this->PDOBindArray($statement, $binded_values);
               $resultat = $statement->execute();
          }
          return $resultat;
     }
     public function insert_chat(Chat $chat):bool {
          $statement = $this->connection->prepare("INSERT INTO chat(message, id_conseiller, id_candidat, dateHeure_message, type_message) VALUES (:message, :destinataire, :expediteur, :dateHeure_message, :type_message)");
          $binded_values = array(
               "message" => $chat->getMessage(),
               "destinataire" => $chat->getDestinataire(),
               "expediteur" =>$chat->getExpediteur(),
               "dateHeure_message" =>$chat->getDateHeureMessage(),
               "type_message" =>$chat->getTypeMessage()
          );
          $this->PDOBindArray($statement, $binded_values);
     
          return $statement->execute();
     }
     public function insert_test($id_candidat, $resultat):bool {
          date_default_timezone_set('Europe/Paris');
          setlocale(LC_TIME, 'fr_FR.utf8','fra');
          $current_time = strftime("%A %d %B %Y %H:%M");
          $statement = $this->connection->prepare("INSERT INTO test_orientation(id_candidat, resultat, date_passation) VALUES (:id_candidat, :resultat, :date_passation)");
          $binded_values = array(
               "id_candidat" => $id_candidat,
               "resultat" => $resultat,
               "date_passation" =>$current_time
          );
          $this->PDOBindArray($statement, $binded_values);
     
          return $statement->execute();
     }
     public function insert_resultat_conseillier($resultat, $id_test):bool {
          $statement = $this->connection->prepare("UPDATE test_orientation.test_orientation set conseille_conseilier=:resultat WHERE id_test=:id_test");
          $binded_values = array(
               "resultat" => $resultat,
               "id_test" => $id_test
          );
          $this->PDOBindArray($statement, $binded_values);
     
          return $statement->execute();
     }
     public function gettingMessage_ByDestinataire($id_destinataire):array {
          $request = $this->connection->query("SELECT * FROM chat WHERE id_conseiller='$id_destinataire'");
          $data_return = $request->fetchAll(PDO::FETCH_CLASS, Chat::class);
          return $data_return;
     }
     public function gettingMessage_ByCandidatAndConseiller($id_candidat, $id_conseillier):array {
          $request = $this->connection->query("SELECT * FROM chat WHERE id_candidat='$id_candidat' AND id_conseiller='$id_conseillier'");
          $data_return = $request->fetchAll(PDO::FETCH_ASSOC);
          return $data_return;
     }
     public function getting_10QuestionsRandom_And_Response(){
          $requete = $this->connection->query("SELECT * FROM questions,reponses WHERE questions.id_questions = reponses.id_questions GROUP BY questions.id_questions LIMIT 10");
          return $requete->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC);
     }
     public function getall_questions():array {
          $request = $this->connection->query("SELECT * FROM questions");
          $donnees = $request->fetchAll(PDO::FETCH_ASSOC);
          return $donnees;
     }
     public function getQuetionsById($id):array {
          $request = $this->connection->query("SELECT * FROM questions WHERE id_questions='$id'");
          $donnees = $request->fetch(PDO::FETCH_ASSOC);
          return $donnees;
     }
     public function getQuetionsBonusById($id):array {
          $request = $this->connection->query("SELECT * FROM questions_bonus WHERE id_bonus='$id'");
          $donnees = $request->fetch(PDO::FETCH_ASSOC);
          return $donnees;
     }
     
     private function PDOBindArray(&$poStatement, &$paArray){
          
          foreach ($paArray as $k=>$v){
               
               @$poStatement->bindValue(':'.$k,$v);
               
          } // foreach
     }
     public  function  get_random_Questions($limit = Database_Operations::LIMIT_QUESTION ) {
          $array = $this->getall_questions();
          shuffle($array);
          
          if ( $limit > 0 ) {
               $array = array_splice($array, 0, $limit);
          }
          return $array;
     }
     
     public function getResponsesByIdQuestion($id_questions)
     {
          $request = $this->connection->query("SELECT * FROM reponses WHERE id_questions='$id_questions'");
          $donnees = $request->fetchAll(PDO::FETCH_ASSOC);
          return $donnees;
     }
     public function getResponsesById($id_reponse)
     {
          $request = $this->connection->query("SELECT * FROM reponses WHERE id_reponse='$id_reponse'");
          $donnees = $request->fetchAll(PDO::FETCH_ASSOC);
          return $donnees;
     }
     public function getResponsesByIdBonus($id_reponse)
     {
          $request = $this->connection->query("SELECT * FROM reponses_bonus WHERE id_reponse_bonus='$id_reponse'");
          $donnees = $request->fetchAll(PDO::FETCH_ASSOC);
          return $donnees;
     }
     public function getResponsesByIdQuestionsBonus($id_questions)
     {
          $request = $this->connection->query("SELECT * FROM reponses_bonus WHERE id_question_bonus='$id_questions'");
          $donnees = $request->fetchAll(PDO::FETCH_ASSOC);
          return $donnees;
     }
     public function getNumberOfBonusQuestions()
     {
          $request = $this->connection->query("SELECT * FROM questions_bonus");
          $count = $request->rowCount();
          return $count;
     }
     
     public function getAlltest_ByCandidat($id_candidat)
     {
          $request = $this->connection->query("SELECT * FROM test_orientation WHERE test_orientation.test_orientation.id_candidat ='$id_candidat'");
          $donnees = $request->fetchAll(PDO::FETCH_ASSOC);
          return $donnees;
     }
     
     
}
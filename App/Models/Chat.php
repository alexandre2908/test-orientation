<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 05/09/2018
 * Time: 13:12
 */

class Chat
{
     private $id_chat;
     private $message;
     private $destinataire;
     private $expediteur;
     private $dateHeure_message;
     private $type_message;
     private $lus_conseiller;
     
     /**
      * @return mixed
      */
     public function getIdChat()
     {
          return $this->id_chat;
     }
     
     /**
      * @param mixed $id_chat
      */
     public function setIdChat($id_chat)
     {
          $this->id_chat = $id_chat;
     }
     
     /**
      * @return mixed
      */
     public function getMessage()
     {
          return $this->message;
     }
     
     /**
      * @param mixed $message
      */
     public function setMessage($message)
     {
          $this->message = $message;
     }
     
     /**
      * @return mixed
      */
     public function getDestinataire()
     {
          return $this->destinataire;
     }
     
     /**
      * @param mixed $destinataire
      */
     public function setDestinataire($destinataire)
     {
          $this->destinataire = $destinataire;
     }
     
     /**
      * @return mixed
      */
     public function getExpediteur()
     {
          return $this->expediteur;
     }
     
     /**
      * @param mixed $expediteur
      */
     public function setExpediteur($expediteur)
     {
          $this->expediteur = $expediteur;
     }
     
     /**
      * @return mixed
      */
     public function getDateHeureMessage()
     {
          return $this->dateHeure_message;
     }
     
     /**
      * @param mixed $dateHeure_message
      */
     public function setDateHeureMessage($dateHeure_message)
     {
          $this->dateHeure_message = $dateHeure_message;
     }
     
     /**
      * @return mixed
      */
     public function getTypeMessage()
     {
          return $this->type_message;
     }
     
     /**
      * @param mixed $type_message
      */
     public function setTypeMessage($type_message)
     {
          $this->type_message = $type_message;
     }
     
     /**
      * @return mixed
      */
     public function getLusConseiller()
     {
          return $this->lus_conseiller;
     }
     
     /**
      * @param mixed $lus_conseiller
      */
     public function setLusConseiller($lus_conseiller)
     {
          $this->lus_conseiller = $lus_conseiller;
     }
     
     
     
}
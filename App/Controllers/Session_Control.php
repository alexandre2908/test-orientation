<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 06/09/2018
 * Time: 00:34
 */
class Session_Control
{
     public static function verify_membre_logged(){
          if (!isset($_SESSION['membres'])){
               header("Location: Membre.login.php");
          }
     }
     public static function verify_candidat_logged(){
          if (!isset($_SESSION['candidat'])){
               header("Location: Login.php");
          }
     }
     public static function giving_membre_username(){
          if (isset($_SESSION['membres'])){
               $session = $_SESSION['membres'][0][0];
               echo $session['username_membre'];
          }else{
               header("Location: Membre.login.php");
          }
     }
     public static function giving_membre_id(){
          if (isset($_SESSION['membres'])){
               $session = $_SESSION['membres'][0][0]['id_membres'];
               return $session;
          }else{
               return NULL;
          }
     }
     public static function printing_membre_id(){
          if (isset($_SESSION['membres'])){
               $session = $_SESSION['membres'][0][0]['id_membres'];
               echo $session;
          }else{
               return NULL;
          }
     }
     
     public static function giving_candidat_username(){
          if (isset($_SESSION['candidat'])){
               $session = $_SESSION['candidat'][0][0];
               echo $session['email_candidat'];
          }else{
               header("Location: Login.php");
          }
     }
     public static function giving_candidat_id(){
          if (isset($_SESSION['candidat'])){
               $session = $_SESSION['candidat'][0][0];
               echo $session['id_candidat'];
          }else{
               header("Location: Login.php");
          }
     }
     public static function return_candidat_id(){
          if (isset($_SESSION['candidat'])){
               $session = $_SESSION['candidat'][0][0];
               return $session['id_candidat'];
          }else{
               header("Location: Login.php");
          }
     }
     
     public static function getall_niveau()
     {
          $niveau_1 = $_SESSION["niveau_1"];
          $niveau_2 = $_SESSION["niveau_2"];
          $niveau_3 = $_SESSION["niveau_3"];
          $all_value = array("niveau_1" =>$niveau_1, "niveau_2"=>$niveau_2, "niveau_3"=>$niveau_3);
          
          return $all_value;
     }
     
     
}
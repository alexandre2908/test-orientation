<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 06/09/2018
 * Time: 15:55
 */
require("../Models/Database_Connections.php");
require("../Models/Database_Operations.php") ;

if ($_POST['message']){
     $operation = new Database_Operations();
     $id_candidat = $_POST['id_candidat'];
     $id_conseillier = $_POST['id_conseillier'];
     $message = $_POST['message'];
     $current_date = date("d/m/Y H:i", strtotime("now"));
     $type_message = "candidat";
     
     $chat = new Chat();
     $chat->setMessage($message);
     $chat->setDestinataire($id_conseillier);
     $chat->setExpediteur($id_candidat);
     $chat->setDateHeureMessage($current_date);
     $chat->setTypeMessage($type_message);
     
     
     $insert =$operation->insert_chat($chat);
     if ($insert){
          echo "done";
     }else{
          echo "null";
     }
}
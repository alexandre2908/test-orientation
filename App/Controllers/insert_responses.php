<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 15/09/2018
 * Time: 15:17
 */

require("../Models/Database_Connections.php");
require("../Models/Database_Operations.php") ;
$operations = new Database_Operations();
//
//$reponses1 = array(
//     array("reponse" =>"Etre bon en sciences ", "niveau_reponse" => "1", "id_question"=>1),
//     array("reponse" =>"Etre bon en sciences et pas trop  mauvais en français ", "niveau_reponse" => "2", "id_question"=>1),
//     array("reponse" =>"Etre premier de la classe ", "niveau_reponse" => "3", "id_question"=>1),
//
//     //autre question
//     array("reponse" =>"Un exercice que vous n’aimez pas, mais il faut passer par là", "niveau_reponse" => "3", "id_question"=>2),
//     array("reponse" =>"Le plaisir de chercher et de trouver une solution", "niveau_reponse" => "1", "id_question"=>2),
//     array("reponse" =>"Une logique à acquérir ", "niveau_reponse" => "2", "id_question"=>2),
//     //autre question
//     array("reponse" =>"Vous le démontez pour comprendre d’où vient le problème ", "niveau_reponse" => "1", "id_question"=>3),
//     array("reponse" =>"Vous essayez de le réparer, mais abandonnez assez vite ", "niveau_reponse" => "2", "id_question"=>3),
//     array("reponse" =>"Vous appelez le réparateur ", "niveau_reponse" => "3", "id_question"=>3),
//     //autre question
//     array("reponse" =>"Savoir travailler en équipe", "niveau_reponse" => "1", "id_question"=>4),
//     array("reponse" =>"Etre spécialiste d’une technologie ", "niveau_reponse" => "1,2", "id_question"=>4),
//     array("reponse" =>"Etre un bon manager", "niveau_reponse" => "2,3", "id_question"=>4),
//     //autre question
//     array("reponse" =>"Quelqu’un qui sait résoudre les problèmes ", "niveau_reponse" => "1", "id_question"=>5),
//     array("reponse" =>"Quelqu’un qui sait poser les problèmes ", "niveau_reponse" => "3", "id_question"=>5),
//     array("reponse" =>"Quelqu’un qui sait tout sur tout ", "niveau_reponse" => "2", "id_question"=>5),
//     //autre question
//     array("reponse" =>"Pas de problème, vous vous adaptez rapidement ", "niveau_reponse" => "1", "id_question"=>6),
//     array("reponse" =>"C’est la panique vous ne savez pas quoi faire ", "niveau_reponse" => "3", "id_question"=>6),
//     array("reponse" =>"Vous n’aimez pas trop changer vos habitudes, mais il le faut, vous essayez de vous adaptez. ", "niveau_reponse" => "2", "id_question"=>6),
//     //autre question
//     array("reponse" =>"Votre timidité vous  empêche de vous exprimer", "niveau_reponse" => "3", "id_question"=>7),
//     array("reponse" =>"Vous appréciez car on  est meilleur a plusieurs pour trouver des bonnes idées", "niveau_reponse" => "1", "id_question"=>7),
//     array("reponse" =>"Vous vous sentez mieux lorsque vous êtes seul(e). ", "niveau_reponse" => "2", "id_question"=>7),
//     //autre question
//     array("reponse" =>"Important, mais pas avant de vous être un peu détendu(e) ", "niveau_reponse" => "1", "id_question"=>8),
//     array("reponse" =>"C’est impossible pour vous ", "niveau_reponse" => "3", "id_question"=>8),
//     array("reponse" =>"Une priorité, vous visez une mention ", "niveau_reponse" => "2", "id_question"=>8),
//     //autre question
//     array("reponse" =>"Chatter avec des amis ", "niveau_reponse" => "3", "id_question"=>9),
//     array("reponse" =>"A chercher  des questions à toutes vos réponses ", "niveau_reponse" => "1", "id_question"=>9),
//     array("reponse" =>"A préparer vos exposes et enrichir vos devoirs ", "niveau_reponse" => "2", "id_question"=>9),
//     //autre question
//     array("reponse" =>"C’est un challenge, vous réfléchissez mieux tout(e) seul(e) ", "niveau_reponse" => "1", "id_question"=>10),
//     array("reponse" =>"Un plaisir. A plusieurs, on est plus créatif ", "niveau_reponse" => "2", "id_question"=>10),
//     array("reponse" =>"C’est une bonne façon de travailler, mais a plusieurs vous avez du mal à être vraiment efficace.", "niveau_reponse" => "3", "id_question"=>10),
//     //autre question
//     array("reponse" =>"Vous êtes ravi(e) car vous détestez la routine et vous vous adaptez vite ", "niveau_reponse" => "2", "id_question"=>11),
//     array("reponse" =>"Vous ne savez pas quoi faire vous demandez conseil ", "niveau_reponse" => "3", "id_question"=>11),
//     array("reponse" =>"Vous analysez la situation avant de prendre rapidement une décision ", "niveau_reponse" => "1", "id_question"=>11),
//     //autre question
//     array("reponse" =>"C’est facile de se trouver un emploi et avoir beaucoup d’argent ", "niveau_reponse" => "1", "id_question"=>12),
//     array("reponse" =>"C’est difficile d’obtenir un emploi intéressant ", "niveau_reponse" => "2", "id_question"=>12),
//     array("reponse" =>"Pas facile de se trouver un emploi, il faut du temps mais les recherches paient toujours.", "niveau_reponse" => "3", "id_question"=>12),
//     //autre question
//     array("reponse" =>"Hors de question. Mais inévitable ", "niveau_reponse" => "3", "id_question"=>13),
//     array("reponse" =>"Nécessaire pour ne pas perdre les pédales. A condition de ne pas culpabiliser ", "niveau_reponse" => "2", "id_question"=>13),
//     array("reponse" =>"Vital, le tout est de ne pas oublier de s’y remettre ", "niveau_reponse" => "1", "id_question"=>13),
//     //autre question
//     array("reponse" =>"C’est variable : parfois il te booste, parfois il te paralyse ", "niveau_reponse" => "2", "id_question"=>14),
//     array("reponse" =>"C’est tout le contraire, il te fait complètement perdre tes moyens ", "niveau_reponse" => "3", "id_question"=>14),
//     array("reponse" =>"Effectivement, la plupart du temps il te sert de stimulant ", "niveau_reponse" => "1", "id_question"=>14),
//     //autre question
//     array("reponse" =>"Améliorer les choses pour qu’elles fonctionnent de façon optimale", "niveau_reponse" => "1", "id_question"=>15),
//     array("reponse" =>"Appendre à travailler d’une façon à prouver son efficacité ", "niveau_reponse" => "2", "id_question"=>15),
//     array("reponse" =>"Improviser la plupart du temps et faire confiance en son instinct ", "niveau_reponse" => "3", "id_question"=>15),
//     //autre question
//     array("reponse" =>"Vous décourage mais ne vous fait pas abandonnez cette façon de faire ", "niveau_reponse" => "2", "id_question"=>16),
//     array("reponse" =>"Vous donne une confiance en vous", "niveau_reponse" => "1", "id_question"=>16),
//     array("reponse" =>"vous fait comprendre qu’il n’existe qu’une seule solution ", "niveau_reponse" => "3", "id_question"=>16),
//     //autre question
//     array("reponse" =>"vous le lui dite de tout suite ", "niveau_reponse" => "2", "id_question"=>17),
//     array("reponse" =>"vous prenez un peu de temps pour analyser la situation en vous informant ", "niveau_reponse" => "1", "id_question"=>17),
//     array("reponse" =>"vous faites une passerelle ", "niveau_reponse" => "3", "id_question"=>17),
//     //autre question
//     array("reponse" =>"Les meilleures décisions sont les décisions collectives ", "niveau_reponse" => "2", "id_question"=>18),
//     array("reponse" =>"Que les meilleures décisions ne sont pas toujours les plus logiques", "niveau_reponse" => "1", "id_question"=>18),
//     array("reponse" =>"Que les meilleures décisions sont individuelles ", "niveau_reponse" => "3", "id_question"=>18),
//     //autre question
//     array("reponse" =>"Vous êtes du genre à vouloir faire accepter votre idée", "niveau_reponse" => "3", "id_question"=>19),
//     array("reponse" =>"Vous confrontez souvent vos points de vue à ceux des autres avant de trancher ", "niveau_reponse" => "1", "id_question"=>19),
//     array("reponse" =>"Vous faites en sorte de ne pas choquer vos interlocuteurs", "niveau_reponse" => "2", "id_question"=>19),
//     //autre question
//     array("reponse" =>"Vous vous détendez en rigolant dans la cour avec des amis ", "niveau_reponse" => "2", "id_question"=>20),
//     array("reponse" =>"Vous en profitez pour prendre de l’avance dans votre travail ", "niveau_reponse" => "1", "id_question"=>20),
//     //autres questions
//     array("reponse" =>"Ce qu’on demande dépasse vos ressources", "niveau_reponse" => "3", "id_question"=>21),
//    array("reponse" =>"Vous pensez plutôt que le délai n’est pas un problème, le nécessaire est que le travail soit bien fait.", "niveau_reponse" => "2", "id_question"=>21),
//    array("reponse" =>"Vous faites ce que vous pouvez  ", "niveau_reponse" => "1", "id_question"=>21),
//    //autres questions
//    array("reponse" =>"Donner des ordres ", "niveau_reponse" => "2", "id_question"=>22),
//    array("reponse" =>"Recevoir des ordres ", "niveau_reponse" => "1,3", "id_question"=>22),
//    array("reponse" =>"Etre dans une équipe de décision ", "niveau_reponse" => "1", "id_question"=>22),
//    //autres questions
//    array("reponse" =>"La compréhension ", "niveau_reponse" => "2,3", "id_question"=>23),
//    array("reponse" =>"Les aptitudes et la détermination ", "niveau_reponse" => "1", "id_question"=>23),
//    array("reponse" =>"La logique applique à la compréhension ", "niveau_reponse" => "1,2", "id_question"=>23),
//    //autres questions
//    array("reponse" =>"avoir une ligne directive qui trace un plan ", "niveau_reponse" => "1", "id_question"=>24),
//    array("reponse" =>"créer  votre propre ligne directive dans le but d’aboutir au même résultat ", "niveau_reponse" => "2", "id_question"=>24),
//    array("reponse" =>"n’est pas avoir  d’une ligne directive ", "niveau_reponse" => "3", "id_question"=>24),
//    //autres questions
//    array("reponse" =>"dans le bruit et le bazar ", "niveau_reponse" => "3", "id_question"=>25),
//    array("reponse" =>"ça dépend  de ce que vous faites ", "niveau_reponse" => "2", "id_question"=>25),
//    array("reponse" =>"L’ordre et le calme vous inspire ", "niveau_reponse" => "1", "id_question"=>25),
//    //autres questions
//    array("reponse" =>"C’est important pour vous de savoir comment les autres voient le monde ", "niveau_reponse" => "1", "id_question"=>26),
//    array("reponse" =>"Pas besoin l’extérieur ne vous apporte rien ", "niveau_reponse" => "3", "id_question"=>26),
//    array("reponse" =>"La communication est un atout important pour vous ", "niveau_reponse" => "1,2", "id_question"=>26),
//    //autres questions
//    array("reponse" =>"Théorie ", "niveau_reponse" => "1,2", "id_question"=>27),
//    array("reponse" =>"Pratique  ", "niveau_reponse" => "3", "id_question"=>27),
//    array("reponse" =>"Théorie + Pratique ", "niveau_reponse" => "1", "id_question"=>27),
//    //autres questions
//    array("reponse" =>"Vous faites attention à ses réactions ", "niveau_reponse" => "1,2", "id_question"=>28),
//    array("reponse" =>"Vous adorez être le centre d’intérêt ", "niveau_reponse" => "3", "id_question"=>28),
//    //autres questions
//    array("reponse" =>"Sociabilité ", "niveau_reponse" => "1,2", "id_question"=>29),
//    array("reponse" =>"La rigueur ", "niveau_reponse" => "1,2", "id_question"=>29),
//    array("reponse" =>"Vous êtes un artiste dans votre façon de faire ", "niveau_reponse" => "3", "id_question"=>29)
//);
//
//
//$operations->insert_arrayResponse($reponses1);
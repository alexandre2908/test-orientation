<?php

require("../Models/Database_Connections.php");
require("../Models/Database_Operations.php") ;


if (isset($_POST['id_candidat'])){
     $operation = new Database_Operations();
     $id_candidat = $_POST['id_candidat'];
     $id_conseillier = $_POST['id_conseillier'];
     $all_messages = $operation->gettingMessage_ByCandidatAndConseiller($id_candidat, $id_conseillier);
     echo json_encode($all_messages);
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 31/08/2018
 * Time: 11:42
 */
require("../Models/Database_Connections.php");
require("../Models/Database_Operations.php") ;
require("../Models/Candidat.php") ;
session_start();

if (isset($_POST['email_candidat']) && isset($_POST['password_candidat'])&& isset($_POST['nom_complet'])){
     $operations = new Database_Operations();
     $username_candidat = $_POST['email_candidat'];
     $password_candidat = $_POST['password_candidat'];
     $nom_complet = $_POST['nom_complet'];
     
     $new_candidat = new Candidat();
     $new_candidat->setEmailCandidat($username_candidat);
     $new_candidat->setPasswordCandidat($password_candidat);
     $new_candidat->setNomComplet($nom_complet);
     
     $test_login = $operations->register_Candidat($new_candidat);
     if ($test_login){
          echo ("done");
     }else{
          echo("null");
     }
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 31/08/2018
 * Time: 11:42
 */
require("../Models/Database_Connections.php");
require("../Models/Database_Operations.php") ;
require("../Models/Membre.php") ;
session_start();

if (isset($_POST['username_membre']) && isset($_POST['password_membre'])){
     $operations = new Database_Operations();
     $username_membre = $_POST['username_membre'];
     $password_membre = $_POST['password_membre'];
     
     $test_login = $operations->login_verify_membres($username_membre, $password_membre);
     if ($test_login){
          $membres = $operations->getOne_membre($username_membre, $password_membre);
          $operations->setMembreActif($membres[0]['id_membres']);
          $_SESSION['membres'] = array($membres);
          echo ("done");
     }else{
          echo("null");
     }
}
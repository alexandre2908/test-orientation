<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 06/09/2018
 * Time: 15:55
 */
require("../Models/Database_Connections.php");
require("../Models/Database_Operations.php") ;

if (isset($_POST['resultat'])){
     $operation = new Database_Operations();
     $resultat = $_POST['resultat'];
     $id_test = $_POST["id_test"];
     
     $insert =$operation->insert_resultat_conseillier($resultat, $id_test);
     if ($insert){
          echo "done";
     }else{
          echo "null";
     }
}
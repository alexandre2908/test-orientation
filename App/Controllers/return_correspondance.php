<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 20/09/2018
 * Time: 10:38
 */

require("../Models/Database_Connections.php");
require("../Models/Database_Operations.php") ;
$operations = new Database_Operations();

$resultat = $_POST["resultat"];

$corresponance = $operations->gall_Correspondences($resultat);

$tableau_finale = array();
foreach ($corresponance as $item){
     $tableau_finale[] = utf8_encode($item["correspondance"]);
}
$json = json_encode($tableau_finale);
echo $json;

<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 31/08/2018
 * Time: 11:42
 */
require("../Models/Database_Connections.php");
require("../Models/Database_Operations.php") ;
require("../Models/Candidat.php") ;
session_start();

if (isset($_POST['email_candidat']) && isset($_POST['password_candidat'])){
     $operations = new Database_Operations();
     $username_candidat = $_POST['email_candidat'];
     $password_candidat = $_POST['password_candidat'];
     
     $test_login = $operations->login_verify_candidats($username_candidat, $password_candidat);
     if ($test_login){
          $candidats = $operations->getOne_candidat($username_candidat, $password_candidat);
          $_SESSION['candidat'] = array($candidats);
          echo ("done");
     }else{
          echo("null");
     }
}
<?php
session_start();
require ("App/Controllers/Session_Control.php");
require ("App/Models/Database_Connections.php");
require ("App/Models/Database_Operations.php");
$operation = new Database_Operations();
$number = $operation->getall_questions();
$number = $number[0]["id_questions"];
$all_test_passed = $operation->getAlltest_ByCandidat(Session_Control::return_candidat_id());

?>


<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from mdbootstrap.com/live/_MDB/templates/Ecommerce/home-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:19:54 GMT -->
<head>
     <!-- Required meta tags always come first -->
     <meta charset="utf-8">
     
     <?php include "Pages/Includes/Css_Include.php";?>

</head>

<body>
<!--Double navigation-->
<header>
     
     <!-- Navbar -->
     <?php include "Pages/Includes/Navbar.php";?>
     <!-- /.Navbar -->

</header>
<!-- /.Double navigation -->

<div class="container mt-5">
    <a href="init_test.php?n=<?php echo $number?>" class="btn btn-success ml-5" >Commencer un nouveau Test</a>
    <div class="row">
       <?php
            foreach ($all_test_passed as $item){
                ?>
                <div class="col-4 mt-5">
                    
                    <div class="card testimonial-card">

                        <!--Bacground color-->
                        <div class="card-up indigo lighten-1">
                        </div>

                        <!--Avatar-->
                        <div class="avatar"><img src="" class="rounded-circle">
                        </div>

                        <div class="card-body">
                            <!--Name-->
                            <h4 class="card-title"><?php Session_Control::giving_candidat_username()?></h4>
                            <hr>
                            <p class="text-info"><?php echo $item["date_passation"]?></p>
                            <?php $passer = explode(":", $item["resultat"] )[0];?>
                            <!--Quotation-->
                            <a class="btn btn-success" href="#"  onclick="openmodalresultat('<?php echo $item["id_test"]?>', '<?php echo $passer?>')"><i class="fa fa-quote-left"></i> Voir Resultat</a>
                        </div>

                    </div>
                </div>
        <?php
            }
       ?>
    </div>
</div>



<div class="modal fade right" id="centralModalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notify modal-info modal-full-height modal-right" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <p class="heading lead">Resultat</p>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="text-center">
                    <ul class="list-group" id="list_correspondance">
                    
                    </ul>
                </div>
                <div class="text-success">
                    <h4 class="text-success">
                        resultat du conseillier
                    </h4>
                    <p id="resultat_conseil" class="text-info"></p>
                </div>
            </div>

            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Fermer</a>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>

<?php
include("Pages/Includes/Footer.php");
?>
<!-- /.Footer -->



<?php
include("Pages/Includes/scripts.php");
?>
<script type="text/javascript" src="js/test_orientation.js"></script>
</body>
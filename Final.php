<?php
session_start();
require ("App/Controllers/Session_Control.php");
require ("App/Models/Database_Connections.php");
require ("App/Models/Database_Operations.php");
$operation = new Database_Operations();
$all_value = Session_Control::getall_niveau();
$max_value = array_keys($all_value, max($all_value));
$operation->insert_test(Session_Control::return_candidat_id(), "".$max_value[0].":".$_SESSION[$max_value[0]]);
$coresspondance = $operation->gall_Correspondences($max_value[0]);
?>


<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from mdbootstrap.com/live/_MDB/templates/Ecommerce/home-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:19:54 GMT -->
<head>
     <!-- Required meta tags always come first -->
     <meta charset="utf-8">
     
     <?php include "Pages/Includes/Css_Include.php";?>

</head>

<body>
<!--Double navigation-->
<header>
     
     <!-- Navbar -->
     <?php include "Pages/Includes/Navbar.php";?>
     <!-- /.Navbar -->

</header>
<!-- /.Double navigation -->



<div class="container mt-5">
    <div class="jumbotron">
        <h3 class="h3-responsive text-center">Suivant les reponses ainsi que le resultat voici nos conseilles sur ce que vous devriez faire :</h3>
        <ul class="list-group">
            <?php foreach ($coresspondance as $item){
                ?>
                 <li class="list-group-item"><?php echo utf8_encode($item["correspondance"])?></li>
                 <?php
            }?>
        </ul>
        <div class="hr-dark mt-3"></div>
        <h5 class="text-success text-center mt-5">Ce test n'etant pas completement fini nous vous recommandons d'entrer en contact avec nos conseilliers</h5>
        <a class="btn btn-success center ml-5 mt-2" href="Liste_Conseillers.php">Contacter Nos Conseilliers</a>
    </div>
</div>

<?php
include("Pages/Includes/Footer.php");
?>
<!-- /.Footer -->

<?php
include("Pages/Includes/scripts.php");
?>
</body>
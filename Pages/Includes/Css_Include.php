<!-- Font Awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Material Design Bootstrap -->
<link href="css/compiled.min.css" rel="stylesheet">

<!-- Customizer -->
<link rel="stylesheet" href="css/customizer.min.css">

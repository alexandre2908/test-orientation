<?php

?>
<nav class="navbar navbar-expand-lg navbar-dark secondary-color-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="Membre.index.php">TestIN</a>
        <ul class="navbar-nav mr-auto mt-lg-0">
            <li class="nav-item active">
                <a class="nav-link" href="Membre.index.php">Accueil <span class="sr-only"></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Candidat_List_test.php">Test D'orientations</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Candidat_List.php">Messages</a>
            </li>
            
        </ul>

        <ul class="navbar-nav ml-auto mt-lg-0">

            <li class="nav-item">
                <p class="nav-item white-text mr-5" >
                    <?php if (isset($_SESSION)){
                        Session_Control::giving_membre_username();
                    } ?>
                </p>
            </li>
            <li class="nav-item">
                <a class="nav-item white-text"
                href="<?php if (isset($_SESSION)){ echo "logout.php";}
                else {
                    echo "Membre.login.php";
                }?>">
                     <?php if (isset($_SESSION)){ echo "Deconnection";}
                     else{
                         echo "Connection";
                     }?>
                </a>
            </li>

        </ul>

    </div>
</nav>

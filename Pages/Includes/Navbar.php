<nav class="navbar navbar-expand-lg navbar-dark indigo">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="index.php">TestIN</a>
        <ul class="navbar-nav mr-auto mt-lg-0">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Test_Orientation.php">Test D'orientation</a>
            </li>
            
             <?php if (isset($_SESSION['candidat'])){
                echo "
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"Liste_Conseillers.php\">Contacter Nos Conseillers</a>
                </li>";
             }
              ?>
        </ul>
        
        <ul class="navbar-nav ">
            
            <li class="nav-item">
                <p class="nav-item white-text mr-5" >
                     <?php if (isset($_SESSION['candidat'])){
                         Session_Control::giving_candidat_username();
                     }?>
                </p>
            </li>
            <li class="nav-item mr-3">
                <a class="nav-item white-text"
                   href="<?php if (isset($_SESSION['candidat'])){ echo "candidat_logout.php";}
                else {
                     echo "Login.php";
                }?>">
                     <?php if (isset($_SESSION['candidat'])){ echo "Se Deconnecter";}
                     else{
                          echo "Se Connection";
                     }?>
                </a>
            </li>
            <?php
                if (!isset($_SESSION['candidat'])){
                    ?>
                    <li class="nav-item">
                        <a class="nav-item white-text" href="Register.php">S'enregistrer</a>
                    </li>
                     <?php
                }
            ?>
            
        </ul>
        
    </div>
</nav>


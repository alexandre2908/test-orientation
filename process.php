<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 18/09/2018
 * Time: 02:32
 */
session_start();
require ("App/Controllers/Session_Control.php");
require ("App/Models/Database_Connections.php");
require ("App/Models/Database_Operations.php");
$operation = new Database_Operations();
if (!isset($_SESSION["niveau_1"]) && !isset($_SESSION["niveau_2"])&& !isset($_SESSION["niveau_3"])){
     $_SESSION["niveau_1"] = 0;
     $_SESSION["niveau_2"] = 0;
     $_SESSION["niveau_3"] = 0;
     
}
$valeur_niveau1 = $_SESSION["niveau_1"];
$valeur_niveau2 = $_SESSION["niveau_2"];
$valeur_niveau3 = $_SESSION["niveau_3"];

$reponse_choisit = $_POST["reponse_choisit"];
$number = $_POST["number"];
$id_candidat = Session_Control::return_candidat_id();
$next = $number+1;
$niveau = $operation->getResponsesById($reponse_choisit);
$niveau = $niveau[0]["niveau_reponse"];
$split_value = explode(",", $niveau);
foreach ($split_value as $value){
     if ($value == "1"){
          $_SESSION["niveau_1"] = $valeur_niveau1 + 5;
     }else if ($value == "2"){
          $_SESSION["niveau_2"] = $valeur_niveau2 + 5;
     }else if ($value == "3"){
          $_SESSION["niveau_3"] = $valeur_niveau3 + 5;
     }
}
if ($number === "".Database_Operations::LIMIT_QUESTION){
     header("Location:bonus.php?n=1");
}else{
     header("Location:newtest_candidat.php?n=".$next."");
}

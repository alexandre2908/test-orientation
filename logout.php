<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 06/09/2018
 * Time: 01:39
 */
require ("App/Controllers/Session_Control.php");
require ("App/Models/Database_Connections.php");
require ("App/Models/Database_Operations.php");
$operation = new Database_Operations();
session_start();
$operation->setMembreInActif(Session_Control::giving_membre_id());
session_unset();
header("Location: Membre.login.php");
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from mdbootstrap.com/live/_MDB/templates/Ecommerce/home-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:19:54 GMT -->
<head>
     <!-- Required meta tags always come first -->
     <meta charset="utf-8">
     
     <?php include "Pages/Includes/Css_Include.php";?>

</head>

<body>
<!--Double navigation-->
<header>
     
     <!-- Navbar -->
     <?php include "Pages/Includes/Navbar.php";?>
     <!-- /.Navbar -->

</header>
<!-- /.Double navigation -->


<div style="margin-left: 30%; margin-top: 10%">
     <div class="card col-5 ml-lg-5" >
          
          <h5 class="card-header info-color white-text text-center py-4">
               <strong>Se Connecter</strong>
          </h5>
          
          <!--Card content-->
          <div class="card-body px-lg-5 pt-0">
               
               <!-- Form -->
               <div class="text-center mt-4">
                    
                    <!-- Email -->
                    <div class="md-form">
                         <input type="email" id="txt_email" class="form-control validate">
                         <label for="txt_email" data-error="wrong" data-success="right">E-mail</label>
                    </div>
                    
                    <!-- Password -->
                    <div class="md-form">
                         <input type="password" id="txt_password" class="form-control validate">
                         <label for="txt_password" data-error="wrong" data-success="right">Password</label>
                    </div>
                    
                    
                    <!-- Sign in button -->
                    <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0 mt-3" id="btn_login">Se Connecter</button>
               
               
               
               </div>
               <!-- Form -->
          
          </div>
     
     </div>
</div>




<?php
include("Pages/Includes/Footer.php");
?>
<!-- /.Footer -->



<?php
include("Pages/Includes/scripts.php");
?>
<script src="js/login_candidat.js" type="text/javascript"></script>
</body>
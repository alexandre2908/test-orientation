<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 18/09/2018
 * Time: 14:08
 */
session_start();
require ("App/Controllers/Session_Control.php");
require ("App/Models/Database_Connections.php");
require ("App/Models/Database_Operations.php");
$operation = new Database_Operations();
$valeur_niveau1 = $_SESSION["niveau_1"];
$valeur_niveau2 = $_SESSION["niveau_2"];
$valeur_niveau3 = $_SESSION["niveau_3"];
$number_questions = $operation->getNumberOfBonusQuestions();


//form data
$reponse_choisit = $_POST["reponse_choisit"];
$number = $_POST["number"];
$id_candidat = Session_Control::return_candidat_id();
$next = $number+1;

//calcul

$niveau = $operation->getResponsesByIdBonus($reponse_choisit);
$cotation = (float) $niveau[0]["cotation_bonus"];
$niveau = $niveau[0]["niveau_bonus"];

$split_value = explode(",", $niveau);
foreach ($split_value as $value){
     if ($value == "1"){
          $_SESSION["niveau_1"] = $valeur_niveau1 + $cotation;
     }else if ($value == "2"){
          $_SESSION["niveau_2"] = $valeur_niveau2 + $cotation;
     }else if ($value == "3"){
          $_SESSION["niveau_3"] = $valeur_niveau3 + $cotation;
     }
}
if ($number === "".$number_questions){
     header("Location:Final.php");
}else{
     header("Location:bonus.php?n=".$next."");
}
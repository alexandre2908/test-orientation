<?php
session_start();
require ("App/Controllers/Session_Control.php");
require ("App/Models/Membre.php");
require ("App/Models/Database_Connections.php");
require ("App/Models/Database_Operations.php");
$operation = new Database_Operations();
$all_membres = $operation->list_membres_connectes();
Session_Control::verify_candidat_logged();
?>


<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from mdbootstrap.com/live/_MDB/templates/Ecommerce/home-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:19:54 GMT -->
<head>
     <!-- Required meta tags always come first -->
     <meta charset="utf-8">
     
     <?php include "Pages/Includes/Css_Include.php";?>

</head>

<body>
<!--Double navigation-->
<header>
     
     <!-- Navbar -->
     <?php include "Pages/Includes/Navbar.php";?>
     <!-- /.Navbar -->

</header>
<!-- /.Double navigation -->

     <div class="container mt-3">
          <h1 class="text-center text-gray-dark">Voici la Listes des Conseillers Disponible</h1>
     </div>

     <div class="row ">
          <?php
          foreach ($all_membres as $membre){
               ?>

              <div class="col-3 mt-5">
                  <div class="card testimonial-card">

                      <!--Bacground color-->
                      <div class="card-up indigo lighten-1">
                      </div>

                      <!--Avatar-->
                      <div class="avatar"><img src="" class="rounded-circle">
                      </div>

                      <div class="card-body">
                          <!--Name-->
                          <h4 class="card-title"><?php echo $membre->getNomMembre()?></h4>
                          <hr>
                          <!--Quotation-->
                          <a class="btn btn-success" href="Chatting.php?conseillier=<?php echo $membre->getIdMembres()?>"><i class="fa fa-quote-left"></i> Chatter Avec Ce Conseille</a>
                      </div>

                  </div>
              </div>
               <?php
          }
          ?>
     </div>


<?php
include("Pages/Includes/Footer.php");
?>
<!-- /.Footer -->



<?php
include("Pages/Includes/scripts.php");
?>
<script src="js/chatting.js" type="text/javascript"></script>
</body>
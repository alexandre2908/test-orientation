<?php
    session_start();
    require ("App/Controllers/Session_Control.php");
    require ("App/Models/Database_Connections.php");
    require ("App/Models/Database_Operations.php");
    $operation = new Database_Operations();
    $id_conseilliers = $_GET['conseillier'];
    $all_messages = $operation->gettingMessage_ByCandidatAndConseiller(Session_Control::return_candidat_id(),$id_conseilliers);
?>


<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from mdbootstrap.com/live/_MDB/templates/Ecommerce/home-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:19:54 GMT -->
<head>
     <!-- Required meta tags always come first -->
     <meta charset="utf-8">
     
     <?php include "Pages/Includes/Css_Include.php";?>
    <link rel="stylesheet" href="css/mycss.css">

</head>

<body>
<!--Double navigation-->
<header>
     
     <!-- Navbar -->
     <?php include "Pages/Includes/Navbar.php";?>
     <!-- /.Navbar -->

</header>
<!-- /.Double navigation -->



<div class="container mt-5">
    <div class="row">
        <div class="col-10" id="chat_container">
            <ul class="list-group" id="list_chat">
            
            </ul>
        </div>
        <div class="col-8 offset-1">
            <div class="md-form mt-5">
                <textarea type="text" id="message_chat" class="md-textarea"></textarea>
                <label for="message_chat">Ecrivez Votre Message</label>
            </div>
            <input id="id_candidat" type="hidden" value="<?php Session_Control::giving_candidat_id() ?>">
            <input id="id_conseillier" type="hidden" value="<?php echo $id_conseilliers?>">
            <button class="btn btn-success" id="btn_envoyer">Envoyer</button>
        </div>
    </div>
</div>





<?php
include("Pages/Includes/Footer.php");
?>
<!-- /.Footer -->



<?php
include("Pages/Includes/scripts.php");
?>
<script src="js/chatting.js" type="text/javascript"></script>
</body>
<?php
session_start();
require ("App/Controllers/Session_Control.php");
require ("App/Models/Database_Connections.php");
require ("App/Models/Database_Operations.php");
$operation = new Database_Operations();
$id_candidat = $_GET['candidat'];
//$all_messages = $operation->gettingMessage_ByCandidatAndConseiller($id_candidat,Session_Control::printing_membre_id());
?>


<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from mdbootstrap.com/live/_MDB/templates/Ecommerce/home-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Sep 2017 15:19:54 GMT -->
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
     
     <?php include "Pages/Includes/Css_Include.php";?>
    <link rel="stylesheet" href="css/mycss.css">

</head>

<body>
<!--Double navigation-->
<header>

    <!-- Navbar -->
     <?php include "Pages/Includes/Navbar_Admin.php";?>
    <!-- /.Navbar -->

</header>
<!-- /.Double navigation -->



<div class="container mt-5">
    <div class="row">
        <div class="col-12" id="chat_container">
            <ul class="list-group" id="list_chat">

            </ul>
        </div>
        <div class="col-8 mt-5 offset-1">
            <div class="md-form">
                <textarea type="text" id="message_chat" class="md-textarea"></textarea>
                <label for="message_chat">Ecrivez Votre Message</label>
            </div>
            <input id="id_conseillier" type="hidden" value="<?php echo Session_Control::giving_membre_id()?>">
            <input id="id_candidat" type="hidden" value="<?php echo $id_candidat?>">
            <button class="btn btn-success" id="btn_envoyer">Envoyer</button>
            
            </div>
    </div>
</div>

<!-- Modal -->


<?php
include("Pages/Includes/Admin.Footer.php");
?>
<!-- /.Footer -->



<?php
include("Pages/Includes/scripts.php");
?>
<script src="js/chatting_membre.js" type="text/javascript"></script>
</body>